<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li class="menu-title">Main</li>
                <li class="active" >
                    <a class="active" href="{{ route('admin.index') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
                </li>

                <li class="submenu">
                    <a href="#"><i class="fa fa-key" aria-hidden="true"></i> <span> Rooms </span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li><a href="{{ route('rooms.index') }}">All Rooms</a></li>
                        <li><a href="{{ route('rooms.create') }}">Add Room</a></li>
                    </ul>
                </li>
                <li class="submenu">
                    <a href="#"><i class="fa fa-cogs" aria-hidden="true"></i> <span> Room Settings </span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li>
                            <a href="{{ route('categories.index') }}"><span>Room Types & Pricing</span></a>
                        </li>
                        <li>
                            <a href="{{ route('amenities.index') }}"><span>Amenities</span></a>
                        </li>
                        <li>
                            <a href="{{ route('bed_types.index') }}"><span>Bed Types</span></a>
                        </li>
                    </ul>
                </li>
                <li class="" >
                    <a class="" href="{{ route('settings.index') }}"><i class="fa fa-wrench"></i> <span>Property Settings </span></a>
                </li>
{{--                --}}

{{--                <li class="submenu">--}}
{{--                    <a href="#"><i class="fa fa-suitcase" aria-hidden="true"></i><span> Booking</span> <span class="menu-arrow"></span></a>--}}
{{--                    <ul style="display: none;">--}}
{{--                        <li><a href="{{ route('bookings.index') }}">All Booking</a></li>--}}
{{--                        --}}{{--                            <li><a href="{{ route('bookings.edit') }}">Edit Booking</a></li>--}}
{{--                        <li><a href="{{ route('bookings.create') }}">Add Booking</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="submenu">--}}
{{--                    <a href="#"><i class="fa fa-users" aria-hidden="true"></i> <span> Customers </span> <span class="menu-arrow"></span></a>--}}
{{--                    <ul style="display: none;">--}}
{{--                        <li><a href="{{ route('customers.index') }}">All customers</a></li>--}}
{{--                        --}}{{--                            <li><a href="{{ route('customers.edit') }}">Edit Customer</a></li>--}}
{{--                        <li><a href="{{ route('customers.create') }}">Add Customer</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                --}}
{{--                <li class="submenu">--}}
{{--                    <a href="#"><i class="fa fa-user"></i> <span> Gallery </span> <span class="menu-arrow"></span></a>--}}
{{--                    <ul style="display: none;">--}}
{{--                        <li><a href="{{ route('gallery.index') }}">All Gallery</a></li>--}}
{{--                        --}}{{--                            <li><a href="{{ route('staff.edit') }}">Edit Staff</a></li>--}}
{{--                        <li><a href="{{ route('gallery.create') }}">Add Gallery</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="submenu">--}}
{{--                    <a href="#"><i class="fa fa-user"></i> <span> Staff </span> <span class="menu-arrow"></span></a>--}}
{{--                    <ul style="display: none;">--}}
{{--                        <li><a href="{{ route('staff.index') }}">All Staff</a></li>--}}
{{--                        --}}{{--                            <li><a href="{{ route('staff.edit') }}">Edit Staff</a></li>--}}
{{--                        <li><a href="{{ route('staff.create') }}">Add staff</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}


{{--                <li class="submenu">--}}
{{--                    <a href="#"><i class="fa fa-user"></i> <span> Events </span> <span class="menu-arrow"></span></a>--}}
{{--                    <ul style="display: none;">--}}
{{--                        <li><a href="{{ route('events.index') }}">All Events</a></li>--}}
{{--                        --}}{{--                            <li><a href="{{ route('staff.edit') }}">Edit Staff</a></li>--}}
{{--                        <li><a href="{{ route('events.create') }}">Add Event</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="submenu">--}}
{{--                    <a href="#"><i class="fa fa-user"></i> <span> Blog </span> <span class="menu-arrow"></span></a>--}}
{{--                    <ul style="display: none;">--}}
{{--                        <li><a href="{{ route('blog.index') }}">All Blog</a></li>--}}
{{--                        --}}{{--                            <li><a href="{{ route('staff.edit') }}">Edit Staff</a></li>--}}
{{--                        <li><a href="{{ route('blog.create') }}">Create Post</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}





            </ul>
        </div>
    </div>
</div>