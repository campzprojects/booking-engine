<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

@include('includes.header')
@yield('css')
<body>

<main class="py-4">
    @yield('content')
</main>

@include('includes.footer')
</body>
<script>
    var base_url = "{{asset('')}}";
</script>
@include('includes.scripts')
@yield('js')
</html>
