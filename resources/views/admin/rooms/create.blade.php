@extends('layouts.main')
@section('content')
    <div class="page-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h4 class="page-title">{{ isset($room) ? 'Edit Room' : 'Add Room' }}</h4>
                </div>
                @if($errors->any())
                    {{--if($count($errors) > 0 )--}}

                    <ul class="list-group">
                        @foreach($errors->all() as $error)

                            <li class="list-group-item text-danger">
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <form method="post"
                          action="{{ isset($room) ? route('rooms.update', $room->id) : route('rooms.store') }}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if(isset($room))
                            {{ method_field('PUT') }}
                        @endif
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Room Number</label>
                                    <input type="number" max="1000" min="1" class="form-control" name="room_number"
                                           value="{{ isset($room) ? $room->room_number : '' }}" type="text">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Room Category</label>
                                    <select name="category_id" class="select">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}"
                                                    @if(isset($room))
                                                    @if( $room->category_id == $category->id)
                                                    selected
                                                @endif
                                                @endif
                                            >  {{ $category->type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Room Name</label>
                                    <input class="form-control" name="name"
                                           value="{{ isset($room) ? $room->name : '' }}" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" id="" cols="30"
                                              rows="3"></textarea>
                                </div>
                            </div>
                        </div>

                        <h5>Beds</h5>
                        <div class="row border-bottom ">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Bed Type</label>
                                    <select name="bed_type_id[0]" class="select">
                                        @foreach($bed_types as $key => $bed_type)
                                            <option value="{{ $bed_type->id }}" {{$key == 0 ? 'selected' : '' }}>
                                                {{ $bed_type->title }}
                                                - {{$bed_type->sleeps}} {{$bed_type->width ? '('.$bed_type->width.' X '.$bed_type->length.')' :''  }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Bed Count</label>
                                    <select name="count[0]" class="select">
                                        @for($x = 1; $x<= $max_bed_count ;$x++)
                                            <option value="{{ $x }}" {{$x == 1 ? 'selected' : ''}}>{{ $x }}</option>
                                        @endfor
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <button type="button" onclick="addBed()" class="btn btn-sm btn-success btn-block">
                                        Add Bed
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="row border-bottom " id="bed_count_addition">

                        </div>

                        <h5>Media</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Main Image</label>
                                    <input type="file" class="form-control" name="main_image">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Other Images</label>
                                    <input type="file" class="form-control" name="images[]" multiple>
                                </div>
                            </div>
                        </div>
                        @if(count($amenities))
                            <h5>Amenities</h5>
                            <div class="row">
                                @foreach($amenities as $key => $amenity)
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label
                                                for="{{str_replace(' ', '_', $amenity->title).$key}}">{{$amenity->title}}</label><input
                                                id="{{str_replace(' ', '_', $amenity->title).$key}}" type="checkbox"
                                                name="amenities[]" value="{{$amenity->title}}">
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <div class="m-t-20 text-center">
                            <button type="submit"
                                    class="btn btn-primary submit-btn">{{ isset($room) ? 'Update' : 'Save' }}</button>
                            <button type="reset" class="btn btn-danger submit-btn">Cancel</button>
                        </div>
                    </form>

                    {{--@endforeach--}}
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        var bed_type = '';
        @foreach($bed_types as $key => $bed_type)
            bed_type += '<option value="{{ $bed_type->id }}" {{$key == 0 ? 'selected' : '' }}>' +
            '{{ $bed_type->title }}' +
            '- {{$bed_type->sleeps}} {{$bed_type->width ? '('.$bed_type->width.' X '.$bed_type->length.')' :''  }}' +
            '</option>';
        @endforeach

        var bed_count = '';
        @for($x = 1; $x<= $max_bed_count ;$x++)
            bed_count += '<option value="{{ $x }}" {{$x == 1 ? 'selected' : ''}}>{{ $x }}</option>';
        @endfor
        var element_id = 1;

        function addBed() {
            var element_class = 'ele-' + element_id;
            var add_bed_component = '<div class="col-md-6 '+element_class+'">\n' +
                '<div class="form-group">\n' +
                '   <label>Bed Type</label>\n' +
                '      <select name="bed_type_id[' + element_id + ']" class="select">\n' +
                bed_type +
                '      </select>\n' +
                '</div>\n' +
                '</div>\n' +
                '<div class="col-md-2 '+element_class+'">\n' +
                '   <div class="form-group">\n' +
                '       <label>Bed Count</label>\n' +
                '           <select name="count[' + element_id + ']" class="select">' +
                bed_count +
                '           </select>' +
                '   </div>\n' +
                '</div>\n' +
                '<div class="col-md-2 '+element_class+'">\n' +
                '   <div class="form-group">\n' +
                '     <label>&nbsp;</label>\n' +
                '     <button type="button" class="btn btn-sm btn-danger btn-block" onclick="removeElement('+element_id+')">Remove</button>\n' +
                '   </div>\n' +
                '</div>';

            $('#bed_count_addition').append(add_bed_component);
            element_id++;
        }

        function removeElement(id){
            var element_class = '.ele-' + id;
            var x = confirm("Are you sure to remove this?");

            if (x){
                $( element_class ).remove();
            }
        }
    </script>
@endsection
