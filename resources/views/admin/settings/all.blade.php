@extends('layouts.main')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-sm-8 col-4">
                    <h4 class="page-title">Settings</h4>
                </div>
                <div class="col-sm-4 col-8 text-right m-b-30">
                    <a href="{{ route('settings.create') }}" class="btn btn-primary btn-rounded pull-right"><i
                                class="fa fa-plus"></i> Settings</a>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-12">
                </div>
            </div>
        </div>
@endsection