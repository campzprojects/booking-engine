@extends('layouts.main')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-sm-8 col-4">
                    <h4 class="page-title">Add Settings</h4>
                </div>
                <div class="col-sm-4 col-8 text-right m-b-30">
                    {{--                    {{dump($settings)}}--}}
                    {{--                    {{dd($setting_metas)}}--}}
                </div>
            </div>
            <div class="row text-center">
                <div class="col-lg-8 offset-lg-2">
                    <form method="post" action="#" enctype="multipart/form-data">
                        {{--                        {{ csrf_field() }}--}}
                        {{--                        @if(isset($room))--}}
                        {{--                            {{ method_field('PUT') }}--}}
                        {{--                        @endif--}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Attribute</label>
                                    <select name="category_id" class="select">
                                        @foreach($setting_metas as $setting_meta)
                                            @php $disabled = ''; @endphp
                                            @foreach($settings as $setting)
                                                @if(($setting->attribute_name == $setting_meta->attribute_name) AND $setting_meta->repetability == 0)
                                                    @php $disabled = 'disabled'; @endphp
                                                @endif
                                            @endforeach
                                            <option {{$disabled}}> {{$setting_meta->attribute_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Attribute Label</label>
                                    <input class="form-control" type="text" name="phone"
                                           value="{{ isset($room) ? $room->phone : '' }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Value</label>
                                    <textarea class="form-control" name="description" id="" cols="30"
                                              rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="m-t-20 text-center">
                            <button type="submit"
                                    class="btn btn-primary submit-btn">{{ isset($room) ? 'Update' : 'Save' }}</button>
                            <button type="reset" class="btn btn-danger submit-btn">Cancel</button>
                        </div>
                    </form>
                    {{--@endforeach--}}
                </div>
            </div>
        </div>
@endsection