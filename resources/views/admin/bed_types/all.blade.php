@extends('layouts.main')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-sm-8 col-4">
                    <h4 class="page-title">Bed Types</h4>
                </div>
                <div class="col-sm-4 col-8 text-right m-b-30">
                    <a href="{{ route('bed_types.create') }}" class="btn btn-primary btn-rounded pull-right"><i
                                class="fa fa-plus"></i> Add Bed Types</a>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped custom-table datatable table-bordered table-condensed"
                               id="data_table">
                            <thead>
                            <tr>
                                <th style="width:5%;">#</th>
                                <th>Title</th>
                                <th>Icon</th>
                                <th>Sleeps</th>
                                <th>Dimensions</th>
                                <th>Status</th>
                                <th class="text-right">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $x = 1; @endphp
                            @foreach($amenities as $amenity)
                                <tr>
                                    <td>{{$x}}</td>
                                    <td>{{$amenity->title}}</td>
                                    <td>{{empty($amenity->icon) ? ' - ' : $amenity->icon}}</td>
                                    <td>{{$amenity->sleeps}}</td>
                                    <td>{{($amenity->width > 0) && ($amenity->length > 0) ? $amenity->width .' X '. $amenity->length .' inch' : ' - '}}</td>
                                    <td>
                                        @if($amenity->status)
                                            <span class="badge badge-success badge-pill">Active</span>
                                        @else
                                            <span class="badge badge-danger badge-pill">Deactive</span>
                                        @endif
                                    </td>
                                    <td>Action</td>
                                </tr>
                                @php $x++; @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection