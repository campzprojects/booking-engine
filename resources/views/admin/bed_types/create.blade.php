@extends('layouts.main')

@section('content')

    <div class="page-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h4 class="page-title">Add Bed Types</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <form method="post"
                          action="{{ isset($category) ? route('bed_types.update',['bed_type'=>$category->id]) : route('bed_types.store') }}">
                        {{ csrf_field() }}
                        @if( isset($category))
                            {{ method_field('PUT') }}
                        @endif
                        <div class="form-group">
                            <label>Title *</label>
                            <input name="title" value="{{ isset($category) ? $category->title: '' }}"
                                   class="form-control" type="text" placeholder="Title" required>
                        </div>
                        <div class="form-group">
                            <label>Icon </label>
                            <input name="icon" value="{{ isset($category) ? $category->icon : '' }}"
                                   class="form-control" type="text" placeholder="Icon">
                        </div>
                        <div class="form-group">
                            <label>Sleeps </label>
                            <input name="sleeps" value="{{ isset($category) ? $category->sleeps : 1 }}"
                                   class="form-control" type="number" placeholder="sleeps" min="1" max="100">
                        </div>
                        <div class="form-group">
                            <label>Description </label>
                            <input name="description" value="{{ isset($category) ? $category->description : '' }}"
                                   class="form-control" type="text" placeholder="Description">
                        </div>
                        <div class="form-group">
                            <label>Width (inch)</label>
                            <input name="width" value="{{ isset($category) ? $category->width : 0 }}"
                                   class="form-control" type="text" placeholder="Width (inch)" min="0" max="100">
                        </div>
                        <div class="form-group">
                            <label>Length (inch)</label>
                            <input name="length" value="{{ isset($category) ? $category->length : 0 }}"
                                   class="form-control" type="number" placeholder="Length (inch)" min="0" max="100">
                        </div>

                        <div class="form-group">
                            <label class="display-block">Bed Type Status</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="pricing_active" value="1"
                                       {{ isset($category) ?  $category->status == 1 ? 'checked' : '' : 'checked' }}>
                                <label class="form-check-label" for="pricing_active">
                                    Active
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="pricing_inactive"
                                       value="0" {{ isset($category) ?  $category->status == 0 ? 'checked' : '' : '' }}>
                                <label class="form-check-label" for="pricing_inactive">
                                    Inactive
                                </label>
                            </div>
                        </div>
                        <div class="m-t-20 text-center">
                            <button type="submit" class="btn btn-primary btn-lg">Publish Bed Type</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection