@extends('layouts.main')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-sm-8 col-4">
                    <h4 class="page-title">Pricing</h4>
                </div>
                <div class="col-sm-4 col-8 text-right m-b-30">
                    <a href="{{ route('categories.create') }}" class="btn btn-primary btn-rounded pull-right"><i
                                class="fa fa-plus"></i> Add Pricing</a>
                </div>
            </div>
            <div class="row text-center">

                @foreach($categories as $category)

                    <div class="col-sm-3 col-md-3 col-lg-3 col-xl-4">
                        <div class="pricing-box">
                            <h3 class="pricing-title">{{ $category->type }}</h3>
                            <p></p>
                            <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Bed Price
                                    <span class="badge-pill">
                                        <b>
                                            <sup>$</sup>{{ $category->price }}
                                        </b>
                                    </span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Breakfirst Price
                                    <span class="badge-pill">
                                        <b>
                                            <sup>$</sup>{{ $category->breakfirst_price }}
                                        </b>
                                    </span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Lunch Price
                                    <span class="badge-pill">
                                        <b>
                                            <sup>$</sup>{{ $category->lunch_price }}
                                        </b>
                                    </span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Dinner Price
                                    <span class="badge-pill">
                                        <b>
                                            <sup>$</sup>{{ $category->dinner_price }}
                                        </b>
                                    </span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Additional Price
                                    <span class="badge-pill">
                                        <b>
                                            <sup>$</sup>{{ $category->additional_price }}
                                        </b>
                                    </span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Status
                                    <span class="badge-pill">
                                        <b>
                                            @if($category->status)
                                                <span class="badge badge-success badge-pill">Active</span>
                                            @else
                                                <span class="badge badge-danger badge-pill">Deactive</span>
                                            @endif
                                        </b>
                                    </span>
                                </li>

                            </ul>
                            <a href="{{ route('categories.edit', $category->id) }}"
                               class="btn btn-primary btn-sm btn-block w-md">Edit</a>
                        </div>
                    </div>
                @endforeach

                <div class="col-sm-6 col-md-4 col-lg-8 col-xl-3">
                    <div class="pricing-box add-pricing">
                        <div class="display-table">
                            <div class="table-cell">
                                <a href="{{ route('categories.create') }}" class="btn add-price-btn btn-rounded"><i
                                            class="fa fa-plus" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection