@extends('layouts.main')

@section('content')

    <div class="page-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h4 class="page-title">Add Pricing</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <form method="post"
                          action="{{ isset($category) ? route('categories.update',['category'=>$category->id]) : route('categories.store') }}">
                        {{ csrf_field() }}
                        @if( isset($category))
                            {{ method_field('PUT') }}
                        @endif
                        <div class="form-group">
                            <label>Pricing Name *</label>
                            <input name="type" value="{{ isset($category) ? $category->type : '' }}"
                                   class="form-control" type="text" placeholder="Single" required>
                        </div>
                        <div class="form-group">
                            <label>Bed only price * </label>
                            <input name="price" value="{{ isset($category) ? $category->price : 0 }}"
                                   class="form-control" type="text" required>
                        </div>
                        <div class="form-group">
                            <label>Breakfirst Price (Per Person)</label>
                            <input name="breakfirst_price"
                                   value="{{ isset($category) ? $category->breakfirst_price : 0 }}" class="form-control"
                                   type="text">
                        </div>
                        <div class="form-group">
                            <label>Lunch Price (Per Person)</label>
                            <input name="lunch_price" value="{{ isset($category) ? $category->lunch_price : 0 }}"
                                   class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label>Dinner Price (Per Person)</label>
                            <input name="dinner_price" value="{{ isset($category) ? $category->dinner_price : 0 }}"
                                   class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label>Additional Charges</label>
                            <input name="additional_price"
                                   value="{{ isset($category) ? $category->additional_price : 0 }}" class="form-control"
                                   type="text">
                        </div>
                        {{--<div class="form-group">--}}
                        {{--<label>Pricing Description</label>--}}
                        {{--<textarea cols="30" rows="6" class="form-control"></textarea>--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <label class="display-block">Pricing Status</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="pricing_active" value="1"
                                       {{ isset($category) ?  $category->status == 1 ? 'checked' : '' : 'checked' }}>
                                <label class="form-check-label" for="pricing_active">
                                    Active
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="pricing_inactive"
                                       value="0" {{ isset($category) ?  $category->status == 0 ? 'checked' : '' : '' }}>
                                <label class="form-check-label" for="pricing_inactive">
                                    Inactive
                                </label>
                            </div>
                        </div>
                        <div class="m-t-20 text-center">
                            <button type="submit" class="btn btn-primary btn-lg">Publish Pricing</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection