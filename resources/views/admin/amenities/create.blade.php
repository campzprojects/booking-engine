@extends('layouts.main')

@section('content')

    <div class="page-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h4 class="page-title">Add Amenities</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <form method="post"
                          action="{{ isset($category) ? route('amenities.update',['amenity'=>$category->id]) : route('amenities.store') }}">
                        {{ csrf_field() }}
                        @if( isset($category))
                            {{ method_field('PUT') }}
                        @endif
                        <div class="form-group">
                            <label>Title *</label>
                            <input name="title" value="{{ isset($category) ? $category->title: '' }}"
                                   class="form-control" type="text" placeholder="Title" required>
                        </div>
                        <div class="form-group">
                            <label>Icon </label>
                            <input name="icon" value="{{ isset($category) ? $category->icon : '' }}"
                                   class="form-control" type="text" placeholder="Icon">
                        </div>

                        <div class="form-group">
                            <label class="display-block">Amenity Status</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="pricing_active" value="1"
                                       {{ isset($category) ?  $category->status == 1 ? 'checked' : '' : 'checked' }}>
                                <label class="form-check-label" for="pricing_active">
                                    Active
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="pricing_inactive"
                                       value="0" {{ isset($category) ?  $category->status == 0 ? 'checked' : '' : '' }}>
                                <label class="form-check-label" for="pricing_inactive">
                                    Inactive
                                </label>
                            </div>
                        </div>
                        <div class="m-t-20 text-center">
                            <button type="submit" class="btn btn-primary btn-lg">Publish Amenity</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection