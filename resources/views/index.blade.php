@extends('layouts.frontend')

@section('css')

@endsection

@section('content')
    <div class="row">
        <div class="col-md-2">
            <img src="{{asset('')}}frontend/images/logo/logo.jpg" alt="">
        </div>
        <div class="col-md-10">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">

                    <div class="item">
                        <img src="{{ asset('') }}frontend/images/slide-2.png" style="width:100%" alt="First slide">
                    </div>
                    <div class="item active">
                        <img src="{{ asset('') }}frontend/images/slide-3.png" style="width:100%" alt="First slide">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="rh  section-booking">
        <!-- Booking Page Begin -->
        <div class="">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-wizard">
                    <!-- Form Wizard -->
                    <form action="{{route('submit')}}" method="post">
                        <!-- Form progress -->
                        {{csrf_field()}}
                        <div class="form-wizard-steps form-wizard-tolal-steps-5">
                            <div class="form-wizard-progress">
                                <div class="form-wizard-progress-line" data-now-value="12.25" data-number-of-steps="4"
                                     style="width: 12.25%;"></div>
                            </div>
                            <!-- Step 1 -->
                            <div class="form-wizard-step active">
                                <div class="form-wizard-step-icon"><i class="fa fa-calendar" aria-hidden="true"></i>
                                </div>
                                <p>Choose Your Date</p>
                            </div>
                            <!-- Step 1 -->
                            <!-- Step 2 -->
                            <div class="form-wizard-step">
                                <div class="form-wizard-step-icon"><i class="fa fa-building-o" aria-hidden="true"></i>
                                </div>
                                <p>Choose Your Room</p>
                            </div>
                            <!-- Step 2 -->
                            <!-- Step 3 -->
                            <div class="form-wizard-step">
                                <div class="form-wizard-step-icon"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i>
                                </div>
                                <p>Reservation</p>
                            </div>
                            <!-- Step 3 -->
                            <!-- Step 4 -->
                            <div class="form-wizard-step">
                                <div class="form-wizard-step-icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                                <p>Confirmation</p>
                            </div>
                            <!-- Step 4 -->
                            <!-- Step 5 -->
                            <div class="form-wizard-step">
                                <div class="form-wizard-step-icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                                <p>Confirmation</p>
                            </div>
                            <!-- Step 5 -->
                        </div>
                        <div class="container">
                            <!-- Form progress -->
                            <!-- Form Step 1 -->
                            <fieldset style="display: block;">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="daterangepicker-result"></div>
                                                <input id="daterangepicker1" type="hidden">
                                                <div id="daterangepicker1-container"
                                                     class="embedded-daterangepicker"></div>
                                            </div>
                                            <input type="hidden" id="start_date" name="arrival_date">
                                            <input type="hidden" id="end_date" name="departure_date">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">

                                        <!-- Check Availability Begin -->
                                        <section class="rh rh-100 check-availability rh-check-availability select-list">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 co-sm-3 col-xs-6 rh-xs-12">
                                                        <h5>Room Type</h5>
                                                        <select class="selectpicker" name="room_type" id="room_type">
                                                            @foreach($room_types as $room_type)
                                                                <option
                                                                    value="{{$room_type->id}}">{{$room_type->type}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 co-sm-3 col-xs-6 rh-xs-12">
                                                        <h5 class="fw-x15">Adults</h5>
                                                        <select class="selectpicker">
                                                            @for($elder = 1; $elder<=$elders_max; $elder++)
                                                                <option
                                                                    value="{{$elder}}">{{$elder}} {{str_plural('Adult', $elder)}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 co-sm-3 col-xs-6 rh-xs-12">
                                                        <h5 class="fw-15">Children</h5>
                                                        <select class="selectpicker">
                                                            @for($child = 1; $child<=$children_max; $child++)
                                                                <option
                                                                    value="{{$child}}">{{$child}} {{str_plural('Child', $child)}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <div class="form-wizard-buttons">
                                                        <div
                                                            class="rh-clear col-lg-3 col-md-3 co-sm-3 col-xs-6 rh-xs-12">
                                                            <a class="rh-check-btn pull-right btn-next disabled"
                                                               href="javascript:;" id="dates-next" >Next</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <!-- Check Availability Close -->
                                    </div>
                                </div>

                            </fieldset>
                            <!-- Form Step 1 -->
                            <!-- Form Step 2 -->
                            <fieldset>
                                <!-- Featured Rooms Begin -->
                                <section class="rh rh-featured-rooms rh-booking-rooms">
                                    <div class="row" id="rooms_all">
                                    </div>
                                </section>
                                <!-- Featured Rooms Close -->

                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous" id="rooms-previous">Previous</button>
                                    <button type="button" class="btn btn-next disabled" id="rooms-next">Next</button>
                                </div>
                            </fieldset>
                            <!-- Form Step 2 -->
                            <!-- Form Step 3 -->
                            <fieldset>
                                <div class="rh rh-reservation-form">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-xs-6 rh-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="First Name" name="first_name">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-6 rh-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Last Name" name="last_name"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-6 rh-xs-12">
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Email" name="email"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-6 rh-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Phone" name="mobile"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-6 rh-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="City" name="city"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-6 rh-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Address" name="address"/>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <textarea cols="12" rows="10" class="form-control"
                                                      placeholder="Special Requirements" name="spacial_requirement"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-wizard-buttons">
                                    <div class="">

                                    </div>
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="button" class="btn btn-next disabled" id="details-next">Next</button>
                                </div>
                            </fieldset>
                            <!-- Form Step 3 -->
                            <!-- Form Step 4 -->
                            <fieldset>
                                <div class="rh-reservation-complate">
                                    <h4>Reservation Complete!</h4>
                                    <p>Details of your reservation request was sent to your email </p>
                                    <p>For more information you can contact us via <a href="contact.html">contact
                                            form</a>
                                        of website</p>
                                </div>
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset>
                            <!-- Form Step 4 -->
                            <!-- Form Step 4 -->
                            <fieldset>
                                <div class="rh-reservation-complate">
                                    <h4>Reservation Complete!</h4>
                                    <p>Details of your reservation request was sent to your email </p>
                                    <p>For more information you can contact us via <a href="contact.html">contact
                                            form</a>
                                        of website</p>
                                </div>
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="submit" class="btn btn-submit">Done</button>
                                </div>
                            </fieldset>
                            <!-- Form Step 4 -->
                        </div>
                    </form>

                    <!-- Form Wizard -->
                </div>
            </div>
        </div>
        <!-- Booking Page Close -->
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function (e) {

        });

    </script>
@endsection

