@extends('layouts.frontend')

@section('css')

@endsection

@section('content')
    <div class="row">
        <div class="col-md-2">
            <img src="{{asset('')}}frontend/images/logo/logo.jpg" alt="">
        </div>
        <div class="col-md-10">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">

                    <div class="item">
                        <img src="{{ asset('') }}frontend/images/slide-2.png" style="width:100%" alt="First slide">
                    </div>
                    <div class="item active">
                        <img src="{{ asset('') }}frontend/images/slide-3.png" style="width:100%" alt="First slide">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="rh  section-booking">
        <!-- Booking Page Begin -->
        <div class="">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-wizard">
                    <!-- Form Wizard -->
                    <form action="{{route('submit')}}" method="post">
                        <!-- Form progress -->
                        {{csrf_field()}}
                        <div class="form-wizard-steps form-wizard-tolal-steps-5">
                            <div class="form-wizard-progress">
                                <div class="form-wizard-progress-line" data-now-value="12.25" data-number-of-steps="4"
                                     style="width: 12.25%;"></div>
                            </div>
                            <!-- Step 1 -->
                            <div class="form-wizard-step active">
                                <div class="form-wizard-step-icon"><i class="fa fa-calendar" aria-hidden="true"></i>
                                </div>
                                <p>Choose Your Date</p>
                            </div>
                            <!-- Step 1 -->
                            <!-- Step 2 -->
                            <div class="form-wizard-step">
                                <div class="form-wizard-step-icon"><i class="fa fa-building-o" aria-hidden="true"></i>
                                </div>
                                <p>Choose Your Room</p>
                            </div>
                            <!-- Step 2 -->
                            <!-- Step 3 -->
                            <div class="form-wizard-step">
                                <div class="form-wizard-step-icon"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i>
                                </div>
                                <p>Reservation</p>
                            </div>
                            <!-- Step 3 -->
                            <!-- Step 4 -->
                            <div class="form-wizard-step">
                                <div class="form-wizard-step-icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                                <p>Confirmation</p>
                            </div>
                            <!-- Step 4 -->
                            <!-- Step 5 -->
                            <div class="form-wizard-step">
                                <div class="form-wizard-step-icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                                <p>Confirmation</p>
                            </div>
                            <!-- Step 5 -->
                        </div>
                        <div class="container">

                            <!-- Form Step 2 -->
                            <fieldset>
                                <!-- Featured Rooms Begin -->
                                <section class="rh rh-featured-rooms rh-booking-rooms">
                                    <div class="row" id="rooms_all">
                                    </div>
                                </section>
                                <!-- Featured Rooms Close -->

                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous" id="rooms-previous">Previous</button>
                                    <button type="button" class="btn btn-next disabled" id="rooms-next">Next</button>
                                </div>
                            </fieldset>
                            <!-- Form Step 2 -->

                        </div>
                    </form>

                    <!-- Form Wizard -->
                </div>
            </div>
        </div>
        <!-- Booking Page Close -->
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function (e) {

        });

    </script>
@endsection

