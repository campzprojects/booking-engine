
<!-- Javascript Files ================================================== -->
<script type="text/javascript" src="{{ asset('') }}frontend/js/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('') }}frontend/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('') }}frontend/js/bootstrap-select.js"></script>
<script type="text/javascript" src="{{ asset('') }}frontend/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{ asset('') }}frontend/js/jquery-ui-date.js"></script>
<script type="text/javascript" src="{{ asset('') }}frontend/js/jquery-ui.js"></script>
<script type="text/javascript" src="{{ asset('') }}frontend/js/slick.min.js"></script>
<!-- Date Javascript Files -->
<script type="text/javascript" src='{{ asset('') }}frontend/js/moment.min.js'></script>
<script type="text/javascript" src='{{ asset('') }}frontend/js/fullcalendar.js'></script>
<!-- Theme Functions -->
<script type="text/javascript" src="{{ asset('') }}frontend/js/form-wizard.js"></script>
<script type="text/javascript" src="{{ asset('') }}frontend/js/themefunction.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="{{ asset('') }}frontend/js/custom_js.js"></script>



