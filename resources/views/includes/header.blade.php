
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Royal Luxury Stay - Online Booking Smart Responsive HTML5 Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Ocean View Beach Resort Booking Engine ">
    <meta name="keywords" content="">

    <meta name="theme-color" content="#ffffff">

    <!-- CSS Files ================================================== -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}frontend/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}frontend/css/jquery-ui-date.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}frontend/css/form-wizard.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <link rel="stylesheet" type="text/css" href="{{ asset('') }}frontend/css/color.css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}frontend/css/style.css"/>
    <style>
        .embedded-daterangepicker .daterangepicker::before,
        .embedded-daterangepicker .daterangepicker::after {
            display: none;
        }
        .embedded-daterangepicker .daterangepicker {
            position: relative !important;
            top: auto !important;
            left: auto !important;
            float: left;
            width: 100%;
            margin-top: 0;
        }
        .embedded-daterangepicker .daterangepicker .drp-calendar {
            width: 50%;
            max-width: 50%;
        }
        .disabled{
            pointer-events: none;
            cursor: not-allowed;
            opacity: 0.5;
        }
    </style>

</head>
