Date
// init daterangepicker
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
if(dd<10){ dd='0'+dd }
if(mm<10){ mm='0'+mm }
var today = '"'+mm+'/'+dd+'/'+yyyy+'"';

var picker = $('#daterangepicker1').daterangepicker({
    "parentEl": "#daterangepicker1-container",
    "autoApply": true,
    "minDate":today
});
// range update listener
picker.on('apply.daterangepicker', function(ev, picker) {
    $("#daterangepicker-result").html('<span class="start-date">Arrival Date : ' + picker.startDate.format('YYYY-MM-DD') + '</span> <span class="end-date"> Departure Date' + picker.endDate.format('YYYY-MM-DD')+ '</span>');

    $('#start_date').val(picker.startDate.format('YYYY-MM-DD'));
    $('#end_date').val(picker.endDate.format('YYYY-MM-DD'));
    $('#dates-next').removeClass("disabled");

});
picker.data('daterangepicker').hide = function () {};
picker.data('daterangepicker').show();

var countCheckedCheckboxes = 0;
// booking setp one
$('#dates-next').click(function (e){
    var selected_room_type = $('#room_type').val();

    $.ajax({
        url: base_url+'api/rooms/get/'+selected_room_type,
        method: 'GET',
        success:function (res){
            $('#rooms_all').html(res);
            checkboxsCount()
        }
    });
});

function checkboxsCount(){
    var $checkboxes = $('#rooms_all input[type="checkbox"]');
    $checkboxes.change(function(){
        countCheckedCheckboxes = $checkboxes.filter(':checked').length;
        if (countCheckedCheckboxes > 0){
            $('#rooms-next').removeClass("disabled");
        }else{
            $('#rooms-next').addClass("disabled");
        }
    });
}

$('#rooms-previous').click(function (){
    countCheckedCheckboxes = 0;
    $('#rooms-next').addClass("disabled");
});
