<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
      'type', 'price','breakfirst_price','lunch_price','dinner_price','additional_price','status'
    ];
    public function room()
    {
        return $this->belongsTo(App\Room);
    }

    public static function getActiveRoomTypes(){
        return Category::where('status',1)->get();
    }
}
