<?php

namespace App\Http\Controllers\Admin;

use App\Amenity;
use App\BedType;
use App\BedTypeRoom;
use Illuminate\Support\Facades\Mail;
use Session;
use App\Room;
use App\Category;
use App\Booking;
use App\Http\Requests\CreateRoomRequest;
use App\Http\Requests\UpdateRoomRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.rooms.all_rooms')
            ->with('rooms', Room::all())
            ->with('booking', Booking::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $room = DB::table('rooms')
            ->join('categories', 'rooms.category_id', '=', 'categories.id')
            ->where('rooms.category_id', '=', 'category.id')
            ->select('categories.*', 'rooms.category_id')
            ->first();
        $bed_types = BedType::where('status', 1)->get();
        $max_bed_count = 5;
        $amenities = Amenity::where('status', 1)->get();

        return view('admin.rooms.create')
            ->with('room', $room)
            ->with('bed_types', $bed_types)
            ->with('amenities', $amenities)
            ->with('max_bed_count', $max_bed_count)
            ->with('categories', Category::all());


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRoomRequest $request)
    {
        if ($request->hasFile('main_image')) {

            $main_image = 'main-' . time() . '.' . $request->main_image->extension();

            $request->main_image->move(public_path('images/rooms'), $main_image);

        }
        $other_images = [];

        if (isset($request->images)) {
            foreach ($request->images as $image) {
                $other_image = 'sub-' . time() . '.' . $image->extension();

                $image->move(public_path('images/rooms'), $other_image);

                array_push($other_images, $other_image);
            }
        }
        $data = array(
            'name' => $request->name,
            'main_image' => $main_image,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'room_number' => $request->room_number,
            'amenities' => json_encode($request->amenities),
            'images' => json_encode($other_images),
        );

        $res = Room::Create($data);

        foreach ($request->bed_type_id as $key => $bed_type) {
            $bed_count = $request->count;

            $bed_room = new BedTypeRoom();
            $bed_room->bed_type_id = $bed_type;
            $bed_room->room_id = $res->id;
            $bed_room->count = $bed_count[$key];
            $bed_room->save();
        }
        if ($res) {
            session::flash('success', 'Room created successfully');
        } else {
            session::flash('danger', 'Something went wrong!');
        }
        return redirect(route('rooms.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        return view('admin.rooms.create')
            ->with('room', $room)
            ->with('categories', Category::all());

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoomRequest $request, Room $room)
    {
        if ($request->hasFile('image')) {
            $room->update([
                'category_id' => $request->category_id,
                'room_number' => $request->room_number,
                'description' => $request->description,
                'bed_count' => $request->bed_count,
                'phone' => $request->phone,
                'image' => $request->image->store('public/images/rooms'),
            ]);

        } else {
            $data = $request->only(['category_id', 'description', 'room_number', 'bed_count', 'phone']);

            $room->update($data);
        }


        session()->flash('success', 'Room updated successfully');

        return redirect(route('rooms.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {

        if (file_exists($room->image)) {
            unlink($room->image);
        }
        $room->delete();

        session()->flash('danger', 'Room deleted successfully');

        return redirect()->back();
    }


    /**
     * Make available the specified resource in storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function makeAvailable($id)
    {

        $rooms = Room::find($id);

        if ($rooms->booking->approve) {
            session()->flash('info', 'Sorry, this room is assigned to a customer');

        } else {
            $rooms->status = true;

            $rooms->save();

        }

        return redirect(route('rooms.index'));
    }

    /**
     * Make unavailable the specified resource in storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function makeUnAvailable($id)
    {
        $rooms = Room::find($id);

        $rooms->status = false;
        $rooms->save();

        session()->flash('success', 'Room successfully made unavailable');

        return redirect(route('rooms.index'));

    }
}
