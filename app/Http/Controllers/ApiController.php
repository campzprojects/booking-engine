<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    //
    public function getRoomsByCategory($category_id)
    {
        $rooms = Room::where('category_id', $category_id)->with(['category', 'beds_types'])->get();

        $html = '';
        if (count($rooms)) {
            foreach ($rooms as $room) {
                $html .= $this->singleRoom($room);
            }
        }

        if ($html == '') {
            $html = 'No rooms found';
        }
        return $html;
    }

    public function singleRoom($room)
    {
        $path = asset('');

        $beds = '';
        $amenities = '';

        if ($room->beds_types) {
            $sleeps = 0;
            $size = '';

            foreach ($room->beds_types as $beds_type) {
                if (!empty($beds_type->beds->width) && !empty($beds_type->beds->length)) {
                    $size = '( ' . $beds_type->beds->width . ' X ' . $beds_type->beds->length . ')';
                }
                if (!empty($beds_type->beds->sleeps)) {
                    $sleeps += $beds_type->beds->sleeps;
                }
                $beds .= '<i class="fa fa-bed" aria-hidden="true"></i> X ' . $beds_type->count . ' ' . $beds_type->beds->title . ' <small class="text-muted">' . $size . '</small><br>';
            }

            $sleeps = '<i class="fa fa-user"></i> X ' . $sleeps . ' Adults';
        }
        if (!empty($room->amenities)) {
            $amenities_array = json_decode($room->amenities);
            foreach ($amenities_array as $amenity) {
                $amenities .= '<li class="go-text-right"><i class="fas fa-check-circle text-success"></i> ' . $amenity . '</li>';
            }
        }
        $single = '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="rh rh-feature-box">
                            <div class="rh-img">
                                <img src="' . $path . 'images/rooms/' . $room->main_image . '"
                                     alt="feature_2">
                                <a href="single-rooms.html"></a>
                            </div>
                            <h4>' . $room->name . '</h4>
                            <div class="feature-detail">

                                <div class="">
                                    <div class="row">
                                        <div class="col-xs-6">' . $beds . '</div>
                                        <div class="col-xs-6">' . $sleeps . '</div>
                                    </div>
                                    <hr>
                                </div>

                                <ul class="main-facility-list go-text-right">' . $amenities . '</ul>
                            </div>
                            <input type="checkbox" value="' . $room->id . '" name="selected_rooms[]" id="select_room_' . $room->id . '"> Select
                        </div>
                     </div>';
        return $single;
    }
}
