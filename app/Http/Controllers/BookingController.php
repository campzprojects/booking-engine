<?php

namespace App\Http\Controllers;

use App\Category;
use App\Event;
use Illuminate\Http\Request;

class BookingController extends Controller
{

    public function index()
    {
        $room_types = Category::getActiveRoomTypes();
//        dd($room_types);
        $elders_max = 5;
        $children_max = 5;
        return view('index')
            ->with('room_types', $room_types)
            ->with('elders_max', $elders_max)
            ->with('children_max', $children_max);
    }
    public function step2()
    {

        return view('step-2');

    }
    public function step3()
    {

        return view('step-3');

    }
    public function step4()
    {

        return view('step-4');

    }


    public function submit(Request $request){
        dd($request->all());
    }
}
