<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BedTypeRoom extends Model
{
    protected $guarded =[];
    protected $table = 'bed_type_rooms';
    public $timestamps  = false;

    protected $with = ['beds'];

    public function beds(){
        return $this->hasOne(BedType::class,'id', 'bed_type_id');
    }
}
