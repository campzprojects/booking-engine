<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Room extends Model
{
    protected $guarded =[];
    public function booking(){
        return $this->hasOne(Booking::class);
    }


    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function beds_types(){
        return $this->hasMany(BedTypeRoom::class, 'room_id','id');
    }
}
